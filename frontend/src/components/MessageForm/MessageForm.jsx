import { useState } from "react";
import SendIcon from "@mui/icons-material/Send";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import { TextField } from "@mui/material";

export default function MessageForm({ onSubmit }) {
  const [message, setMessage] = useState("");

  const onInputChange = (e) => {
    const message = e.target.value;
    setMessage(message);
  };

  const onFormSubmit = (e) => {
    onSubmit({ message });
    setMessage("");
  };

  const onKeyPress = (event) => {
    if (event.key === "Enter") {
      onFormSubmit();
    }
  };

  return (
    <FormControl sx={{ width: "100%", p: 1 }} variant="filled">
      <TextField
        size="small"
        onChange={onInputChange}
        value={message}
        onKeyPress={onKeyPress}
        InputProps={{
          endAdornment: (
            <IconButton onClick={onFormSubmit}>
              <SendIcon />
            </IconButton>
          ),
        }}
      />
    </FormControl>
  );
}
