import styled from "@emotion/styled";
import { Box } from "@mui/material";

const FormWrapper = styled(Box)`
  position: relative;
  /* width: 500; */
  /* max-width: 100%; */
  margin-top: 2px;
`;

const Form = styled.form`
  min-height: 35px;
  position: relative;
`;

const FormInput = styled.input`
  width: 100%;
  font-size: 14px;
  padding-top: 7px;
  padding-bottom: 7px;
  padding-right: 33px;
  outline: none;
  border-width: 1px;
  border-color: grey;
  transition: border-color 200ms linear;

  &:focus {
    border-color: grey;
  }
`;

const SendBtn = styled.button`
  position: absolute;
  right: 1px;
  top: 1px;
  min-height: 30px;
  border: none;
  color: #808080ae;
  transition: color 200ms linear;

  &:hover,
  &:focus {
    color: #454545;
  }
`;

export { FormWrapper, Form, FormInput, SendBtn };
