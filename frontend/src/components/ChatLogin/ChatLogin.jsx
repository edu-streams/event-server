import { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";

const ChatLogin = ({ open, handleClose, onLoginFormSubmit }) => {
  const [name, setName] = useState("");

  const nameHandleChange = (e) => {
    const userName = e.target.value;

    setName(userName);
  };

  const btnClickHandler = () => {
    onLoginFormSubmit(name);
    handleClose();
    setName("");
  };

  return (
    <>
      <Dialog
        open={open}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Введи своє ім'я"}</DialogTitle>
        <DialogContent>
          <TextField
            id="outlined-basic"
            type="text"
            placeholder=""
            variant="outlined"
            onChange={nameHandleChange}
            size="small"
            fullWidth
            value={name}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={btnClickHandler}>До чату</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ChatLogin;
