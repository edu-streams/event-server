import styled from "@emotion/styled";
import Box from "@mui/material/Box";

const ChatWrapper = styled(Box)`
  overflow-y: scroll;
  ::-webkit-scrollbar {
    width: 4px;
  }
  ::-webkit-scrollbar-thumb {
    background-color: rgba(158, 158, 158, 1);
    border-radius: 2px;
  }
`;

export { ChatWrapper };
