import ChatMessage from "components/ChatMessage/ChatMessage";
import { ChatWrapper } from "./ChatBoard.styled";
import { useRef, useEffect } from "react";

export default function ChatBoard({ items, name }) {
  const chatWrapperRef = useRef(null);
  useEffect(() => {
    const chatWrapper = chatWrapperRef.current;
    if (chatWrapper) {
      chatWrapper.scrollTop = chatWrapper.scrollHeight;
    }
  }, [items]);

  return (
    <ChatWrapper
      ref={chatWrapperRef}
      height={"100%"}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 1,
        p: 1,
      }}
    >
      {items.map((item) => (
        <ChatMessage key={item.id} item={item} name={name} />
      ))}
    </ChatWrapper>
  );
}
