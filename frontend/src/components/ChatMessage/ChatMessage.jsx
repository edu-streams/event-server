import { Avatar, Box } from "@mui/material";
import { MessageWrapper, UserName, Message } from "./ChatMessage.styled";

export default function ChatMessage({ item }) {
  return (
    <MessageWrapper type={item.type} sx={{ maxWidth: "100%" }}>
      {item.type === "they" && (
        <Box sx={{ display: "flex", gap: 1, alignItems: "center" }}>
          {item.userAvatar && <Avatar alt="Remy Sharp" src={item.userAvatar} />}
          <UserName>{item.userName}:</UserName>
        </Box>
      )}
      {item.type === "me" && <UserName>Я:</UserName>}
      <Message type={item.type}>{item.message}</Message>
    </MessageWrapper>
  );
}
