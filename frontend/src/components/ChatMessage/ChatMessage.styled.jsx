import styled from "@emotion/styled";
import { Box, Typography } from "@mui/material";

const MessageWrapper = styled(Box)`
  display: flex;
  align-items: center;
  gap: 8px;

  padding: 5px;

  background-color: ${({ type }) => (type === "they" ? "white" : "#8080805c")};
  border-radius: 4px;
`;

const UserName = styled(Typography)`
  color: grey;
  font-weight: 600;
`;

const Message = styled(Typography)`
  text-align: ${({ type }) => (type === "they" ? "right" : "left")};
`;

export { MessageWrapper, UserName, Message };
