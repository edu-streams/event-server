import { Box, Button } from "@mui/material";
import { socket } from "../../services/socket";
import { useEffect, useState, useCallback } from "react";
import MessageForm from "components/MessageForm/MessageForm";
import ChatLogin from "../../components/ChatLogin/ChatLogin";
import ChatBoard from "../../components/ChatBoard/ChatBoard";
import { nanoid } from "nanoid";
import * as jose from "jose";

export default function EventPageBlock() {
  const [messageList, setMessageList] = useState([]);
  const [userName, setUserName] = useState("");
  const [userAvatar, setUserAvatar] = useState("");

  useEffect(() => {
    const token = localStorage.getItem("logto:asnhh0px905tmbmdwlqm1:idToken");
    if (token) {
      const claims = jose.decodeJwt(token);
      setUserName(claims.name);
      setUserAvatar(claims.picture);
    }

    socket.on("chat-message", (message) => {
      const parsedMessage = JSON.parse(message);

      setMessageList((prevState) => {
        if (prevState.find((user) => user.id === parsedMessage.id)) {
          return prevState;
        }
        return [...prevState, { ...parsedMessage, type: "they" }];
      });
    });
  }, []);

  const sendMessage = (message) => {
    let msg = JSON.stringify(message);
    socket.emit("chat-message", msg);
  };

  const addMessage = useCallback(
    ({ message }) => {
      setMessageList((prevState) => {
        const newMessage = {
          userName,
          message,
          id: nanoid(8),
          type: "me",
          userAvatar,
        };
        sendMessage(newMessage);
        return [...prevState, newMessage];
      });
    },
    [userName]
  );

  return (
    <Box element="div" sx={{ height: `calc(100vh - 56px)` }}>
      <ChatBoard items={messageList} />

      {userName && <MessageForm onSubmit={addMessage} />}
    </Box>
  );
}
