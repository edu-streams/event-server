const { Server } = require("socket.io");
const {instrument} = require("@socket.io/admin-ui");

const io = new Server({
    // serveClient: false,
    cors: {
        origin: [
            "https://admin.socket.io",
            "http://localhost:3000",
            "https://edu-streams.org",
            "https://stage.edu-streams.org"
        ],
        // credentials: true
    },
    path: "/event-server/"
});

instrument(io, {
    auth: false,
    mode: "development",
});
let socketIsActive = false;

io.on("connection", (socket) => {
    console.log(`connect ${socket.id}`);

    socket.on("chat-message", (message) => {
        socket.broadcast.emit("chat-message", message);
    });

    socket.onAny((e, data) => {
        console.log(`${e}: ${data}`);

        socket.broadcast.emit(e, data);
    });


    socket.on("disconnect", (reason) => {
        console.log(`disconnect ${socket.id} due to ${reason}`);
    });
});

const port = process.env.PORT || 4000;
io.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
